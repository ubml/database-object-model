/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.configuration.datatype;

import io.iec.edp.caf.databaseobject.api.configuration.FileUtils;
import io.iec.edp.caf.databaseobject.api.configuration.funcmapping.DatabaseFuncConfiguration;
import io.iec.edp.caf.databaseobject.api.helper.DboConfigurationHelper;

import java.io.IOException;
import java.util.List;

/**
 * 字段类型映射配置加载
 *
 * @author liu_wei
 */
public class DBODataTypeConfigurationLoader {
    private static String fileName = FileUtils.getSqlConfigPath();
    private static String sectionName = "DataTypeConverter";

    private static List<DatabaseObjectDataTypeConfiguration> databaseObjectConfigurations;

    private static List<DatabaseObjectDataTypeConfiguration> getDatabaseObjectConfigurations() {
        return databaseObjectConfigurations;
    }

    private static void setDatabaseObjectConfiguration(List<DatabaseObjectDataTypeConfiguration> value) {
        databaseObjectConfigurations = value;
    }

    /**
     * 构造函数
     */
    public DBODataTypeConfigurationLoader() throws IOException {
        GetDatabaseObjectConfigurations();
        //GSPConfigurationSource.Current.AddSectionChangeHandler(MetadataServiceConfigSetting.fileName, MetadataServiceConfigSetting.SectionName, ConfigurationChanged);
    }

    /**
     * 获取配置文件数据
     */
    public static List<DatabaseObjectDataTypeConfiguration> GetDatabaseObjectConfigurations() throws IOException {
        if (getDatabaseObjectConfigurations() == null) {
            DboConfigurationHelper helper = new DboConfigurationHelper();
            List<DatabaseObjectDataTypeConfiguration> confs = null;

            confs = helper.getDataTypeConfiguration(fileName, sectionName);
            setDatabaseObjectConfiguration(confs);
            return getDatabaseObjectConfigurations();
        } else {
            return getDatabaseObjectConfigurations();
        }
    }

    public static DatabaseObjectDataTypeConfiguration GetDatabaseObjectConfiguration(String dataType) throws IOException {
        GetDatabaseObjectConfigurations();
        if (getDatabaseObjectConfigurations() != null && !getDatabaseObjectConfigurations().isEmpty()) {
            for (DatabaseObjectDataTypeConfiguration data : getDatabaseObjectConfigurations()) {
                if (data.getDBODataType().equals(dataType)) {
                    return data;
                }
            }
            return null;
        } else {
            return null;
        }
    }


}
