/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * 临时表
 *
 * @author liu_wei
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DatabaseObjectTempTable extends DatabaseObjectTable implements Serializable {

    public DatabaseObjectTempTable() {
        this(UUID.randomUUID().toString());
    }

    private DatabaseObjectTempTable(String dboId) {
        this.setId(dboId);
        this.setType(DatabaseObjectType.TempTable);
        this.setColumns(new ArrayList<>());
        this.setIndexes(new ArrayList<>());
        this.setPrimaryKey(new ArrayList<>());
    }

    /**
     * 对象类型
     */
    @JsonProperty(value = "Type")
    private DatabaseObjectType type = DatabaseObjectType.values()[6];

    @Override
    public DatabaseObjectType getType() {
        return type;
    }

    /**
     * 对象是否多语
     */
    @JsonProperty("isI18nObject")
    private boolean isI18nObject;


    /**
     * 克隆
     *
     * @return 数据库对象临时表
     */
    @Override
    public DatabaseObjectTempTable clone() {
        DatabaseObjectTempTable table = new DatabaseObjectTempTable(this.getId());
        table.setCode(this.getCode());
        table.setName(this.getName());
        table.setPkName(getPkName());
        table.setBusinessObjectId(getBusinessObjectId());
        table.setLastModifiedTime(this.getLastModifiedTime());
        if (this.getHasNameRule()) {
            table.setDboTableNameRule((DBOTableNameRule) this.getDboTableNameRule().clone());
        }
        table.setDescription(this.getDescription());
        //table.setI18NObject(isI18NObject);
        table.setType(this.getType());
        table.setVersion(this.getVersion());
        for (DatabaseObjectColumn column : this.getColumns()) {
            table.getColumns().add(column.clone());
        }
        for (DatabaseObjectIndex index : this.getIndexes()) {
            table.getIndexes().add(index.clone());
        }
        if (this.getMultiLanguageColumns() != null && this.getMultiLanguageColumns().size() > 0) {
            table.getMultiLanguageColumns().addAll(this.getMultiLanguageColumns());
        }
        if (this.getPrimaryKey() != null && this.getPrimaryKey().size() > 0) {
            table.getPrimaryKey().addAll(this.getPrimaryKey());
        }
        return table;
    }
}
