/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


/**
 * 数据库对象索引实体
 * <ramark>考虑之后是否要添加条件描述及索引关联字段上添加升序、降序描述等</ramark>
 *
 * @author liu_wei
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DatabaseObjectIndex extends AbstractDatabaseObject implements Serializable {
    /**
     * 构造函数
     */
    public DatabaseObjectIndex() {
        this(UUID.randomUUID().toString());
    }

    private DatabaseObjectIndex(String indexId) {
        this.setId(indexId);
        this.setColumns(new ArrayList<>());
    }

    /**
     * 是否主键索引
     * <see cref="bool"/>
     */
    @JsonProperty("ifPrimary_Key")
    private boolean ifPrimary_Key;

    /**
     * 是否聚集索引
     * <see cref="bool"/>
     */
    @JsonProperty("isCluster_Constraint")
    private boolean isCluster_Constraint;

    /**
     * 是否唯一索引
     */
    @JsonProperty("isUnique_Constraint")
    private boolean isUnique_Constraint;

    /**
     * 索引关联的列
     */
    private List<DatabaseObjectColumn> columns;

    public final List<DatabaseObjectColumn> getColumns() {
        return columns;
    }

    public final void setColumns(List<DatabaseObjectColumn> value) {
        columns = value;
    }

    /**
     * 克隆
     *
     * @return 数据库对象视图
     */
    @Override
    public final DatabaseObjectIndex clone() {
        DatabaseObjectIndex index = new DatabaseObjectIndex(this.getId());
        index.setCode(this.getCode());
        index.setName(this.getName());
        index.setDescription(this.getName());
        index.setIfPrimary_Key(this.ifPrimary_Key);
        index.setCluster_Constraint(this.isCluster_Constraint);
        index.setUnique_Constraint(this.isUnique_Constraint);
        index.setType(this.getType());
        index.setVersion(this.getVersion());
        if (this.getColumns() != null && this.getColumns().size() > 0) {
            for (DatabaseObjectColumn item : this.getColumns()) {
                index.getColumns().add(item.clone());
            }
        }
        return index;
    }
}
