/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.service;

import io.iec.edp.caf.databaseobject.api.entity.ScopeType;
import io.iec.edp.caf.databaseobject.api.entity.TempTableContext;
import io.iec.edp.caf.database.Database;

/**
 * @author liu_wei
 */
public interface IDatabaseObjectTempTableService {

    /**
     * 创建固定列临时表
     * @param database 数据库连接
     * @param type 临时表类型
     * @param dboCode dboCode
     * @return 临时表信息
     */
    TempTableContext createFixedTable(Database database, ScopeType type, String dboCode);

    /**
     * 创建非固定列临时表
     * @param database 数据库连接
     * @param type 临时表类型
     * @param dboCode dboCode
     * @return 临时表信息
     */
    TempTableContext createNoFixedTable(Database database, ScopeType type, String dboCode);
}
