/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.service;

import io.iec.edp.caf.databaseobject.api.entity.AbstractDatabaseObject;
import io.iec.edp.caf.databaseobject.api.entity.DBInfo;
import io.iec.edp.caf.databaseobject.api.entity.DboDeployResultInfo;
import io.iec.edp.caf.databaseobject.api.entity.DimensionValueEntity;

import java.util.List;
import java.util.Map;

/**
 * DBO部署服务
 *
 * @author zhaoleitr
 */
public interface IDatabaseObjectDeployService {

    /**
     * 部署单个DBO
     *
     * @param databaseObject DBO实体
     * @param dimensionInfo  维度信息
     */
    void deployDatabaseObject(AbstractDatabaseObject databaseObject, Map<String, String> dimensionInfo);

    /**
     * 部署单个DBO到指定服务单元
     *
     * @param databaseObject DBO实体
     * @param dimensionInfo  维度信息
     * @param su  服务单元
     */
    void deployDatabaseObject(AbstractDatabaseObject databaseObject, Map<String, String> dimensionInfo, String su);

    /**
     * 部署多个DBO
     *
     * @param databaseObjects DBO实体列表
     * @param dimensionValue 维度信息
     * @return 部署结果
     */
    DboDeployResultInfo deployDatabaseObjects(List<AbstractDatabaseObject> databaseObjects, Map<String, String> dimensionValue);

    /**
     * 部署多个DBO到指定服务单元
     *
     * @param databaseObjects DBO实体列表
     * @param dimensionValue 维度信息
     * @param su  服务单元
     * @return 部署结果
     */
    DboDeployResultInfo deployDatabaseObjects(List<AbstractDatabaseObject> databaseObjects, Map<String, String> dimensionValue, String su);


    /**
     * 部署多个DBO到指定库
     *
     * @param databaseObjects DBO实体列表
     * @param dimensionValue 维度信息
     * @param dbInfo  数据库信息
     * @return 部署结果
     */
    DboDeployResultInfo deployDatabaseObjects(List<AbstractDatabaseObject> databaseObjects, Map<String, String> dimensionValue, DBInfo dbInfo);

    /**
     * 部署指定路径下DBO
     *
     * @param dboPath       dbo存放路径
     * @param dimensionInfo 维度信息
     */
    void deployDboList(String dboPath, Map<String, String> dimensionInfo);

    /**
     * 部署指定路径下DBO
     *
     * @param dboPath        dbo存放路径
     * @param dimensionInfos 维度信息
     */
    void deployDboWithDimensionList(String dboPath, Map<String, List<String>> dimensionInfos);

    /**
     * 部署指定路径下DBO到指定库
     *
     * @param dboPath       dbo存放路径
     * @param dimensionInfo 维度信息
     * @param dbInfo        数据库信息
     */
    void deployDboList(String dboPath, Map<String, String> dimensionInfo, DBInfo dbInfo);

    /**
     * 部署指定路径下DBO到指定库
     *
     * @param dboPath        dbo存放路径
     * @param dimensionInfos 维度信息
     * @param dbInfo         数据库信息
     */
    void deployDboWithDimensionList(String dboPath, Map<String, List<String>> dimensionInfos, DBInfo dbInfo);


    /**
     * 根据新语言，修改已有的多语DBO
     *
     * @param languages 语言
     */
    void modifyI18nDatabaseObjectsByNewLanguage(List<String> languages);

}
