/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 数据库对象表名规则实体
 *
 * @author liu_wei
 */
@Data
public class DBOTableNameRule implements Serializable {
    public DBOTableNameRule() {
        this.setDimensionStrategyWithOrder(new ArrayList<>());
    }

    /**
     * 规则标识
     */
    private String id;

    /**
     * 规则标识
     */
    private String code;
    /**
     * 规则名称
     */
    private String name;

    /**
     * 是否同步结构
     */
    @JsonProperty("IsSynStructure")
    private boolean isSynStructure;

    /**
     * 带顺序维度策略列表
     */
    private List<DimensionStrategyWithOrder> dimensionStrategyWithOrder;

    @Override
    public Object clone() {
        DBOTableNameRule rule = new DBOTableNameRule();
        rule.setId(this.getId());
        rule.setCode(this.getCode());
        rule.setName(this.getName());
        rule.setSynStructure(this.isSynStructure);
        for (DimensionStrategyWithOrder strategy : dimensionStrategyWithOrder) {
            rule.dimensionStrategyWithOrder.add((DimensionStrategyWithOrder) strategy.clone());
        }
        return rule;
    }
}
