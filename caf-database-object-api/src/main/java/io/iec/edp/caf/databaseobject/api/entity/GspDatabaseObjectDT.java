/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * @author liu_wei
 */
@Data
@Entity
@Table(name = "GSPDatabaseObjectVersion")
public class GspDatabaseObjectDT {

    /**
     * 唯一标识
     */
    @Id
    @Column(length = 36)
    private String id;

    /**
     * 编号
     */
    private String code;

    /**
     * 名称
     */
    private String name;

    /**
     * 类型
     */
    private int type;

    /**
     * 对象实体内容
     */
    private String content;

    /**
     * 业务对象字段关联属性
     */
    private String businessObjectId;

    /**
     * 版本
     */
    private String version;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 最后修改人
     */
    private String lastModifier;

    /**
     * 创建时间
     */
    private LocalDateTime createdTime = LocalDateTime.MIN;
    /**
     * 最后修改时间
     */
    private LocalDateTime lastModifiedTime = LocalDateTime.MIN;
}
