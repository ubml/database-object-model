/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.context;

import io.iec.edp.caf.databaseobject.api.entity.DatabaseObjectColumn;

import java.util.ArrayList;

/**
 * 数据库对象保留字(时间戳字段，租户标识字段)
 *
 * @author liu_wei
 */
public class DatabaseObjectReservedWords {
    private static ArrayList<String> reservedWords;

    /**
     * 数据库保留字列表
     * <see cref="List{T}"/>
     * <see cref="string"/>
     */
    public static ArrayList<String> getReservedWords() {
        if (reservedWords == null || reservedWords.size() <= 0) {
            reservedWords = getReservedWordsFromConfig();
        }
        return reservedWords;
    }


    /**
     * 判断字符串是否是数据库对象保留字
     *
     * @param word 单词
     * @return 如果是数据库保留字，返回true，否则，返回false
     */
    public static boolean isReservedWord(String word) {
        return getReservedWords().contains(word);
    }

    private static ArrayList<String> getReservedWordsFromConfig() {
        ArrayList<String> reservedWords = new ArrayList<>();
        ArrayList<DatabaseObjectColumn> timeStampColumns = ReservedColumns.getTimeStampColumns();
        if (timeStampColumns != null && !timeStampColumns.isEmpty()) {
            for (DatabaseObjectColumn column : timeStampColumns) {
                reservedWords.add(column.getCode());
            }
        }
        if (ReservedColumns.getTenantIdColumn() != null) {
            reservedWords.add(ReservedColumns.getTenantIdColumn().getCode());
        }
        return reservedWords;
    }
}
