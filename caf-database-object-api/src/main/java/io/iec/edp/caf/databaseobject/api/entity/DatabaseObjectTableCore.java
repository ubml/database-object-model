/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * 数据库对象表核心内容实体
 *
 * @author liu_wei
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DatabaseObjectTableCore extends AbstractDatabaseObject implements Serializable {


    private List<DatabaseObjectColumn> columns;

    /**
     * 数据库表字段列表
     */
    public List<DatabaseObjectColumn> getColumns() {
        if (this.columns == null) {
            this.columns = new ArrayList<>();
        }
        return columns;
    }

    public void setColumns(List<DatabaseObjectColumn> value) {
        this.columns = value;
    }

    /**
     * 根据字段ID获取字段
     *
     * @param columnId 字段ID
     * @return 字段实体
     */
    public DatabaseObjectColumn getColumnById(String columnId) {
        Optional<DatabaseObjectColumn> columnsOpt = this.getColumns().stream().filter((item) -> item.getId().equals(columnId)).findFirst();
        return columnsOpt.orElse(null);
    }

    /**
     * 根据字段编号获取字段
     *
     * @param columnCode 字段编号
     * @return 字段实体
     */
    public DatabaseObjectColumn getColumnByCode(String columnCode) {
        Optional<DatabaseObjectColumn> columnsOpt = this.getColumns().stream().filter((item) -> item.getCode().toLowerCase().equals(columnCode.toLowerCase())).findFirst();
        return columnsOpt.orElse(null);
    }

}
