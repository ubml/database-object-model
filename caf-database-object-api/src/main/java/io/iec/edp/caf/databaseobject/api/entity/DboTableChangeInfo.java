/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.entity;

import lombok.Data;

import java.util.List;

/**
 * Dbo增量实体类
 *
 * @author liu_wei
 */
@Data
public class DboTableChangeInfo {

    /**
     * 增加的列
     */
    private List<DatabaseObjectColumn> addedColumn;

    /**
     * 增加的索引
     */
    private List<DatabaseObjectIndex> addedIndex;

    /**
     * 变化的索引
     */
    private List<DatabaseObjectIndex> changedIndex;

    /**
     * 变化的列
     */
    private List<DatabaseObjectColumn> changedColumn;

    /**
     * 变化的列详细信息
     */
    private List<ColumnChangeOperation> changedColumnOperations;

    /**
     * Dbo表命名规则
     */
    private DBOTableNameRule dboTableNameRule;

    /**
     * 描述
     */
    private String description;

    /**
     * 是否为历史表
     */
    private boolean isSynHis;

    /**
     * 是否使用TimeStamp
     */
    private boolean isUsingTimeStamp;

    /**
     * 新增的多语列
     */
    private List<String> newMulityLanguageColumns;

    /**
     * 主键名
     */
    private String pkName;

    /**
     * 主键
     */
    private List<String> primaryKey;

    /**
     * 名称
     */
    private String name;

    /**
     * 是否为多语DBO
     */
    private boolean isI18nObject;

    /**
     * 版本
     */
    private String version;
}
