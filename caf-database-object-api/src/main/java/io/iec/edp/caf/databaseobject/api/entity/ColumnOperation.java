/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.entity;

import lombok.Data;

/**
 * 列的操作行为
 *
 * @author renmh
 */
@Data
public class ColumnOperation {
    /**
     * 操作类型,1:修改类型，2：修改长度 3:修改默认值
     */
    private int operationType;
    /**
     * 类型变化前的旧值
     */
    private Object oldValue;
    /**
     * 类型变化后的新值
     */
    private Object newValue;
}
