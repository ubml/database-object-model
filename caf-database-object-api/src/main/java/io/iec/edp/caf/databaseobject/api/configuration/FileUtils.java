/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.configuration;

import io.iec.edp.caf.commons.runtime.CafEnvironment;
import org.springframework.boot.configurationprocessor.json.JSONObject;

import java.io.*;
import java.nio.file.Paths;

/**
 * 文件操作集
 *
 * @author liu_wei
 */
public class FileUtils {

    private static final String CAF_DBO_TABLE_CONFIG = "config/platform/common/caf_dbo_tablename.json";
    private static final String CAF_DBO_SLQ_CONFIG = "config/platform/common/caf_dbosqlextend.json";

    private static String tableConfigPath;

    public static String isMappingOpen = "";

    public static String isUTF8ExtOpen = "";

    public static String getTableConfigPath() {
        if (tableConfigPath == null) {
            tableConfigPath = Paths.get(CafEnvironment.getServerRTPath(), "config/platform/common/caf_dbo_tablename.json").toString();
        }
        return tableConfigPath;
    }

    private static String sqlConfigPath;

    public static String getSqlConfigPath() {
        if (sqlConfigPath == null) {
            sqlConfigPath = Paths.get(CafEnvironment.getServerRTPath(), "config/platform/common/caf_dbosqlextend.json").toString();
        }
        return sqlConfigPath;
    }

    public static String fileRead(String path) throws IOException {
        String encoding = "UTF-8";
        File file = new File(path);
        Long filelength = file.length();
        byte[] filecontent = new byte[filelength.intValue()];

        FileInputStream in = new FileInputStream(file);
        in.read(filecontent);
        in.close();

        //判断有没有utf-8 bom头。有则去除。
        String fileContents = new String(filecontent, encoding);
        if (fileContents.startsWith("\ufeff")) {

            fileContents = fileContents.substring(1);

        }
        return fileContents;
    }

    public static boolean isExportOpen() {
        String configPath = getSqlConfigPath();
        File file = new File(configPath);
        String exportOpen = "";
        try {
            String content = org.apache.commons.io.FileUtils.readFileToString(file, "UTF-8");
            JSONObject jsonObject = new JSONObject(content);
            exportOpen = (jsonObject.getString("isExportOpen").toLowerCase());
        } catch (Exception e) {
            exportOpen = "false";
        }
        return ("true").equals(exportOpen);
    }

    public static boolean isMappingOpen() {
        if(!"".equals(isMappingOpen)){
            return "true".equals(isMappingOpen);
        }else{
            String configPath = getSqlConfigPath();
            File file = new File(configPath);
            try {
                String content = org.apache.commons.io.FileUtils.readFileToString(file, "UTF-8");
                JSONObject jsonObject = new JSONObject(content);
                isMappingOpen = jsonObject.getString("isMappingOpen");
                if(null == isMappingOpen || "".equals(isMappingOpen)){
                    isMappingOpen = "false";
                }
            } catch (Exception e) {
                isMappingOpen = "false";
            }
            return "true".equals(isMappingOpen);
        }
    }

    public static boolean isUTF8ExtOpen() {
        if(!"".equals(isUTF8ExtOpen)){
            return "true".equals(isUTF8ExtOpen);
        }else{
            String configPath = getSqlConfigPath();
            File file = new File(configPath);
            try {
                String content = org.apache.commons.io.FileUtils.readFileToString(file, "UTF-8");
                JSONObject jsonObject = new JSONObject(content);
                isUTF8ExtOpen = jsonObject.getString("isUTF8ExtOpen");
                if(null == isUTF8ExtOpen || "".equals(isUTF8ExtOpen)){
                    isUTF8ExtOpen = "false";
                }
            } catch (Exception e) {
                isUTF8ExtOpen = "false";
            }
            return "true".equals(isUTF8ExtOpen);
        }
    }

    private static String reservedWordOpen;

    public static boolean getReservedWordOpen()
    {
        if(reservedWordOpen == null)
        {
            String configPath = getSqlConfigPath();
            File file = new File(configPath);
            try {
                String content = org.apache.commons.io.FileUtils.readFileToString(file, "UTF-8");
                JSONObject jsonObject = new JSONObject(content);
                if(jsonObject.isNull("isReservedWordOpen"))
                {
                    reservedWordOpen = "true";
                }
                reservedWordOpen = (jsonObject.getString("isReservedWordOpen").toLowerCase());
            } catch (Exception e) {
                reservedWordOpen = "true";
            }
        }
        return ("true").equals(reservedWordOpen);
    }

}
