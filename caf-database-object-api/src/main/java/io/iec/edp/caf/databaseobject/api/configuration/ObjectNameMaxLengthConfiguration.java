/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.configuration;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zhaoleitr
 */
@Data
public class ObjectNameMaxLengthConfiguration implements Serializable {
    /**
     * 表名最大长度
     */
    private int tableNameMaxLength;

    /**
     * 字段名最大长度
     */
    private int columnNameMaxLength;

    /**
     * 主键名最大长度
     */
    private int pkNameMaxLength;

    /**
     * 索引名最大长度
     */
    private int indexNameMaxLength;

    /**
     * 约束名最大长度
     */
    private int constraintNameMaxLength;
}
