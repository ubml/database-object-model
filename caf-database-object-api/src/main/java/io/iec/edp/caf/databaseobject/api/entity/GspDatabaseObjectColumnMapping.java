package io.iec.edp.caf.databaseobject.api.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 数据库列对象映射实体
 *
 * @author michael xue
 */
@Data
@Entity
@Table(name = "GSPColumnMapping")
public class GspDatabaseObjectColumnMapping {

    /**
     * 唯一标识
     */
    @Id
    @Column(length = 36)
    private String id;


    /**
     * dboId
     */
    private String dboId;


    /**
     * columnId
     */
    private String columnId;

    /**
     * columnIdMapping
     */
    private String columnIdMapping;
}
