/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.*;

/**
 * 数据库对象列实体
 *
 * @author liu_wei
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DatabaseObjectColumn extends AbstractDatabaseObject implements Serializable {
    /**
     * 构造函数
     */
    public DatabaseObjectColumn() {
        this(UUID.randomUUID().toString());
    }

    private DatabaseObjectColumn(String colId) {
        this.setId(colId);
    }

    /**
     * 字段数据类型
     */
    private DataType DataType;
    /**
     * 字段数据类型
     */
    private DataType DataTypeStr;

    public final String getDataTypeStr() {
        return String.valueOf(this.getDataType());
    }

    /**
     * 字段长度
     */
    private int length;

    /**
     * 字段精度
     */
    private int precision;

    /**
     * 字段小数位数
     */
    private int scale;

    /**
     * 字段默认值
     */
    private String defaultValue;
    /**
     * 是否主键
     */
    @JsonProperty("ifPrimaryKey")
    private boolean ifPrimaryKey;

    /**
     * 是否可为空
     */
    @JsonProperty("isNullable")
    private boolean isNullable;

    /**
     * 是否唯一
     */
    @JsonProperty("isUnique")
    private boolean isUnique;

    /**
     获取对应的GSPDbDataTyp

     @return
     */
//	public final GSPDbDataType GetGSPDbDataType()
//	{
//		return DataTypeConverter.getInstance().ConvertDataTypeToGSPDbDataType(this.getDataType());
//	}

    /**
     * 唯一约束名
     */
    private String ucName;

    /**
     * 克隆
     *
     * @return 数据库对象表
     * <see cref="DatabaseObjectColumn"/>
     */
    @Override
    public final DatabaseObjectColumn clone() {
        DatabaseObjectColumn column = new DatabaseObjectColumn(this.getId());
        column.setCode(this.getCode());
        column.setName(this.getName());
        column.setDataType(this.getDataType());
        column.setLength(this.getLength());
        column.setPrecision(this.getPrecision());
        column.setScale(this.getScale());
        column.setDefaultValue(this.getDefaultValue());
        column.setUnique(this.isUnique);
        column.setIfPrimaryKey(this.ifPrimaryKey);
        column.setNullable(this.isNullable);
        column.setType(DatabaseObjectType.Column);
        column.setUcName(this.getUcName());
        column.setVersion(this.getVersion());
        column.setDescription(this.getDescription());
        return column;
    }
}
