/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.configuration;


import io.iec.edp.caf.databaseobject.api.helper.DboConfigurationHelper;

import java.io.*;
import java.util.List;

/**
 * 配置文件加载
 *
 * @author liu_wei
 */
public class TableNameConfigurationLoader implements Serializable {
    private static String fileName = FileUtils.getTableConfigPath();
    private static String sectionName = "TableNameConfiguration";

    private static List<TableNameConfiguration> DatabaseObjectConfigurations;

    private static List<TableNameConfiguration> getDatabaseObjectConfigurations() {
        return DatabaseObjectConfigurations;
    }

    private static void setDatabaseObjectConfigurations(List<TableNameConfiguration> value) {
        DatabaseObjectConfigurations = value;
    }


    /**
     * 构造函数
     *
     * @throws IOException 异常
     */
    public TableNameConfigurationLoader() throws IOException {
        GetDatabaseObjectConfigurations();
    }

    /**
     * 获取配置文件数据
     */
    private static List<TableNameConfiguration> GetDatabaseObjectConfigurations() throws IOException {
        if (getDatabaseObjectConfigurations() == null || getDatabaseObjectConfigurations().size() <= 0) {
            DboConfigurationHelper helper = new DboConfigurationHelper();
            List<TableNameConfiguration> configuration = helper.getTableNameConfigurations(fileName, sectionName);
            setDatabaseObjectConfigurations(configuration);

            return getDatabaseObjectConfigurations();
        } else {
            return getDatabaseObjectConfigurations();
        }
    }


    /**
     * 获取数据库对象配置信息
     *
     * @param ruleCode 规则编号
     * @return 表名配置
     */
    protected final TableNameConfiguration GetDatabaseObjectConfigurationData(String ruleCode) throws IOException {
        GetDatabaseObjectConfigurations();
        if (getDatabaseObjectConfigurations() != null && !getDatabaseObjectConfigurations().isEmpty()) {
            for (TableNameConfiguration data : getDatabaseObjectConfigurations()) {
                if (data.getRuleCode().equals(ruleCode)) {
                    return data;
                }
            }
            return null;
        } else {
            return null;
        }
    }

}
