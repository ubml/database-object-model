/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.io.*;
import java.time.LocalDateTime;

/**
 * 数据库对象基类
 *
 * @author liu_wei
 */
@Data
public class AbstractDatabaseObject implements Cloneable, Serializable {
    /**
     * 对象唯一标识
     */

    private String id;

    /**
     * 对象编号
     */
    private String code;


    /**
     * 对象名称
     */
    private String name;

    /**
     * 对象类型
     */
    @JsonProperty(value = "Type")
    private DatabaseObjectType type = DatabaseObjectType.values()[0];

    public DatabaseObjectType getType() {
        return type;
    }

    public void setType(DatabaseObjectType value) {
        type = value;
    }

    /**
     * 对象类型
     */
    private String typeStr;

    public String getTypeStr() {
        return String.valueOf(this.getType());
    }

    /**
     * 版本（保存自动生成，用于部署时判断是否变更）
     * <see cref="string"/>
     */
    private String version;

    /**
     * 对象说明
     */
    private String description;

    /**
     * 对象说明
     */
    @JsonIgnore
    private LocalDateTime lastModifiedTime;

    /**
     * 克隆
     *
     * @return 数据库对象表
     */
    @Override
    public AbstractDatabaseObject clone() {
        ObjectOutputStream os = null;
        ObjectInputStream ois = null;
        AbstractDatabaseObject target = null;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            os = new ObjectOutputStream(bos);
            os.writeObject(this);

            ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
            ois = new ObjectInputStream(bis);
            target = (AbstractDatabaseObject) ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                ois.close();
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return target;
    }
}
