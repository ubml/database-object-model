/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.entity;

import io.iec.edp.caf.dimension.api.entity.DimensionEntity;
import lombok.Data;

import java.io.Serializable;

/**
 * 带顺序的维度策略
 *
 * @author liu_wei
 */
@Data
public class DimensionStrategyWithOrder implements Serializable {
    /**
     * 维度策略
     */
    private DimensionEntity dimensionEntity;

    /**
     * 顺序
     */
    private int order;

    @Override
    public Object clone() {
        DimensionStrategyWithOrder strategy = new DimensionStrategyWithOrder();
        strategy.setOrder(this.getOrder());
        strategy.setDimensionEntity((DimensionEntity) this.getDimensionEntity().clone());
        return strategy;
    }
}
