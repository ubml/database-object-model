/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.configuration;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * 数据库对象全局配置文件对应的实体类
 *
 * @author liu_wei
 */
public class DatabaseObjectConfiguration implements Serializable {
    /**
     * 数据库类型编号
     */
    @JsonProperty("DBTypeCode")
    private String DBTypeCode;

    public final String getDBTypeCode() {
        return DBTypeCode;
    }

    public final void setDBTypeCode(String value) {
        DBTypeCode = value;
    }

    /**
     * 数据库类型名称
     */
    @JsonProperty("DBTypeName")
    private String DBTypeName;

    public final String getDBTypeName() {
        return DBTypeName;
    }

    public final void setDBTypeName(String value) {
        DBTypeName = value;
    }

    /**
     * 数据库对象SQL反射器
     */
    @JsonProperty("SQLReflector")
    private SQLReflector SQLReflector;

    public final SQLReflector getSQLReflector() {
        return SQLReflector;
    }

    public final void setSQLReflector(SQLReflector value) {
        SQLReflector = value;
    }
}
