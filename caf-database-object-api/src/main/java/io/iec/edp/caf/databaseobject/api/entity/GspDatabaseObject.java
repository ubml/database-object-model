/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 数据库对象信息实体
 *
 * @author liu_wei
 */
@Data
@Entity
@Table(name = "GSPDatabaseObject")
public class GspDatabaseObject {

    public GspDatabaseObject(){}

    public GspDatabaseObject(String id, String code, String name, int type) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.type = type;
        this.createdTime=null;
        this.lastModifiedTime=null;
    }

    /**
     * 唯一标识
     */
    @Id
    @Column(length = 36)
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getIsFiscalTable() {
        return isFiscalTable;
    }

    public void setIsFiscalTable(String isFiscalTable) {
        this.isFiscalTable = isFiscalTable;
    }

    public String getIsI18NObject() {
        return isI18NObject;
    }

    public void setIsI18NObject(String isI18NObject) {
        this.isI18NObject = isI18NObject;
    }

    public String getTenantIDColumnCode() {
        return tenantIDColumnCode;
    }

    public void setTenantIDColumnCode(String tenantIDColumnCode) {
        this.tenantIDColumnCode = tenantIDColumnCode;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getBusinessObjectId() {
        return businessObjectId;
    }

    public void setBusinessObjectId(String businessObjectId) {
        this.businessObjectId = businessObjectId;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public LocalDateTime getLastModifiedTime() {
        return lastModifiedTime;
    }

    public void setLastModifiedTime(LocalDateTime lastModifiedTime) {
        this.lastModifiedTime = lastModifiedTime;
    }


    /**
     * 编号
     */
    private String code;

    /**
     * 名称
     */
    private String name;

    /**
     * 类型
     */
    private int type;

    /**
     * 是否年度表
     */
    private String isFiscalTable;

    /**
     * 是否多语
     */
    private String isI18NObject;

    /**
     * 租户标识字段编号
     */
    private String tenantIDColumnCode;

    /**
     * 版本
     */
    private String version;

    /**
     * 对象实体内容
     * 此处不能添加text注解，否则会出现content记录变成数字的错误
     */
    private String content;

    @Lob
    @Column(columnDefinition = "TEXT")
    public String getContent() {
        return content;
    }

    /**
     * 业务对象字段关联属性
     */
    private String businessObjectId;

    /**
     * 表规则Id
     */
    private String ruleId;

    /**
     * 表规则Code
     */
    private String ruleCode;

    /**
     * 创建时间
     */
    private LocalDateTime createdTime = LocalDateTime.MIN;
    /**
     * 最后修改时间
     */
    private LocalDateTime lastModifiedTime = LocalDateTime.MIN;
}
