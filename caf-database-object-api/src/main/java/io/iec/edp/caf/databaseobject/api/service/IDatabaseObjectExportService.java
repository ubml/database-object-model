/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.service;

import io.iec.edp.caf.databaseobject.api.entity.AbstractDatabaseObject;
import io.iec.edp.caf.databaseobject.api.entity.DBInfo;

import java.util.List;

/**
 * @author liu_wei
 */
public interface IDatabaseObjectExportService {

    /**
     * 是否在IDE中暴露数据库导出DBO的功能
     *
     * @return 是否
     */
    boolean isExportOpen();

    /**
     * 获取当前数据库所有表名
     *
     * @return 表名列表
     */
    List<String> getAllTablesName(DBInfo info);

    /**
     * 根据表名获取DBO实体
     *
     * @param tablesName       表名列表
     * @param businessObjectId 业务对象Id
     * @param info             数据库连接信息
     * @return dbo实体列表
     */
    List<AbstractDatabaseObject> getDatabaseObjectsByTablesName(List<String> tablesName, String businessObjectId, DBInfo info);

    /**
     * 根据表名获取DBO实体
     *
     * @param tableName        表名
     * @param dboName          DBO名
     * @param businessObjectId 业务对象Id
     * @param info             数据库连接信息
     * @return DBO实体
     */
    AbstractDatabaseObject getDatabaseObjectByTableName(String tableName, String dboName, String businessObjectId, DBInfo info);
}
