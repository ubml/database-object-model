/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.configuration;

import io.iec.edp.caf.databaseobject.api.configuration.DatabaseObjectConfiguration;
import io.iec.edp.caf.databaseobject.api.configuration.funcmapping.DatabaseFuncConfiguration;
import io.iec.edp.caf.databaseobject.api.helper.DboConfigurationHelper;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * 配置加载器
 *
 * @author liu_wei
 */
public class DatabaseObjectConfigurationLoader implements Serializable {
    private static String fileName = FileUtils.getSqlConfigPath();
    private static String sectionName = "DatabaseObjectSQLConfiguration";

    private static ArrayList<DatabaseObjectConfiguration> DatabaseObjectConfigurations;

    private static ArrayList<DatabaseObjectConfiguration> getDatabaseObjectConfigurations() {
        return DatabaseObjectConfigurations;
    }

    private static void setDatabaseObjectConfigurations(ArrayList<DatabaseObjectConfiguration> value) {
        DatabaseObjectConfigurations = value;
    }


    /**
     * 构造函数
     */
    public DatabaseObjectConfigurationLoader() {
        GetDatabaseObjectConfigurations();
        //GSPConfigurationSource.Current.AddSectionChangeHandler(MetadataServiceConfigSetting.fileName, MetadataServiceConfigSetting.SectionName, ConfigurationChanged);
    }


    /**
     * 获取配置文件数据
     *
     * @return 配置列表
     */
    private static ArrayList<DatabaseObjectConfiguration> GetDatabaseObjectConfigurations() {
        if (getDatabaseObjectConfigurations() == null || getDatabaseObjectConfigurations().size() <= 0) {
            DboConfigurationHelper helper = new DboConfigurationHelper();
            ArrayList<DatabaseObjectConfiguration> confs = null;

            try {
                confs = helper.getSqlReflectConfiguration(fileName, sectionName);
            } catch (IOException e) {
                throw new RuntimeException("获取数据库对象配置信息报错", e);
            }

            setDatabaseObjectConfigurations(confs);
            return getDatabaseObjectConfigurations();
        } else {
            return getDatabaseObjectConfigurations();
        }
    }

    /**
     * 获取数据库对象配置信息
     *
     * @param typeName 类型名称
     * @return 配置
     */
    protected final DatabaseObjectConfiguration GetDatabaseObjectConfigurationData(String typeName) {
        GetDatabaseObjectConfigurations();
        if (getDatabaseObjectConfigurations() != null && !getDatabaseObjectConfigurations().isEmpty()) {
            for (DatabaseObjectConfiguration data : getDatabaseObjectConfigurations()) {
                if (data.getDBTypeCode().toLowerCase().equals(typeName.toLowerCase())) {
                    return data;
                }
            }
            return null;
        } else {
            return null;
        }
    }
}
