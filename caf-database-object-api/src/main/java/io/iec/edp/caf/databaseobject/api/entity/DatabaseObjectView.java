/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.entity;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * 视图
 *
 * @author zhaoleitr
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DatabaseObjectView extends DatabaseObjectTableCore implements Serializable {
    public DatabaseObjectView() {
        this.setMultiLanguageColumns(new ArrayList<>());
        this.setDefinationWithDbType(new ArrayList<>());
        this.setDboTableNameRule(new DBOTableNameRule());
    }

    /**
     * 业务对象字段关联属性
     */
    private String businessObjectId;

    private List<String> multiLanguageColumns;

    /**
     * 定义
     */
    private String defination;


    private List<ViewDefination> definationWithDbType;

    /**
     * 对象是否多语
     */
    @JsonProperty("isI18nObject")
    private boolean isI18nObject;


    /**
     * 数据库对象表名规则
     */
    private DBOTableNameRule dboTableNameRule;

    private boolean hasNameRule;

    /**
     * 对象类型
     */
    @JsonProperty(value = "Type")
    private DatabaseObjectType type = DatabaseObjectType.values()[1];

    @Override
    public DatabaseObjectType getType() {
        return type;
    }

    /**
     * DBO是否包含表名规则的只读属性
     */
    public final boolean getHasNameRule() {
        return getDboTableNameRule() != null && getDboTableNameRule().getDimensionStrategyWithOrder() != null && !getDboTableNameRule().getDimensionStrategyWithOrder().isEmpty();
    }

    /**
     * 根据字段ID获取字段
     *
     * @param columnId 字段ID
     * @return 字段实体
     */
    @Override
    public final DatabaseObjectColumn getColumnById(String columnId) {
        Optional<DatabaseObjectColumn> columnsOpt = this.getColumns().stream().filter((item) -> item.getId().equals(columnId)).findFirst();
        return columnsOpt.orElse(null);
    }

    /**
     * 根据字段编号获取字段
     *
     * @param columnCode 字段编号
     * @return 字段实体
     */
    @Override
    public final DatabaseObjectColumn getColumnByCode(String columnCode) {
        Optional<DatabaseObjectColumn> columnsOpt = this.getColumns().stream().filter((item) -> item.getCode().toLowerCase().equals(columnCode.toLowerCase())).findFirst();
        return columnsOpt.orElse(null);
    }

    @Override
    public DatabaseObjectView clone() {
        DatabaseObjectView view = new DatabaseObjectView();
        view.setId(this.getId());
        view.setCode(this.getCode());
        view.setName(this.getName());
        view.setBusinessObjectId(getBusinessObjectId());
        if (this.getHasNameRule())
            view.setDboTableNameRule((DBOTableNameRule) this.getDboTableNameRule().clone());
        view.setDescription(this.getDescription());
        view.setType(this.getType());
        view.setVersion(this.getVersion());
        view.setI18nObject(this.isI18nObject);
        view.setLastModifiedTime(this.getLastModifiedTime());

        view.setDefination(this.getDefination());
        if (this.getColumns() != null && this.getColumns().size() > 0) {
            for (DatabaseObjectColumn column : this.getColumns()) {
                view.getColumns().add(column.clone());
            }
        }
        if (this.getMultiLanguageColumns() != null && this.getMultiLanguageColumns().size() > 0) {
            view.getMultiLanguageColumns().addAll(this.getMultiLanguageColumns());
        }
        if (this.getDefinationWithDbType() != null && this.getDefinationWithDbType().size() > 0) {
            for (ViewDefination defination : this.getDefinationWithDbType()) {
                view.getDefinationWithDbType().add((ViewDefination) defination.clone());
            }
        }
        return view;
    }
}
