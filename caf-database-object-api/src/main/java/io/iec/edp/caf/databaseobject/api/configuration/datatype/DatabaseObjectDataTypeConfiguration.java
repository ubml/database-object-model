/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.configuration.datatype;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.*;

/**
 * 数据库对象类型映射配置
 *
 * @author liu_wei
 */
public class DatabaseObjectDataTypeConfiguration implements Serializable {
    /**
     * DBO中字段类型
     */
    @JsonProperty("DBODataType")
    private String DBODataType;

    public final String getDBODataType() {
        return DBODataType;
    }

    public final void setDBODataType(String value) {
        DBODataType = value;
    }

    /**
     * 数据库类型映射关系列表
     */
    private ArrayList<DataTypeMapping> DataTypeMappings;

    public final ArrayList<DataTypeMapping> getDataTypeMappings() {
        return DataTypeMappings;
    }

    public final void setDataTypeMappings(ArrayList<DataTypeMapping> value) {
        DataTypeMappings = value;
    }
}
