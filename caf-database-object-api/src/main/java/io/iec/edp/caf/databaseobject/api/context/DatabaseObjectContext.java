/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.context;

/**
 * DBO上下文
 *
 * @author zhaoleitr
 */
public class DatabaseObjectContext {
    /**
     * dbo文件后缀
     */
    public static String getSuffix() {
        return ".dbo";
    }


    /**
     * 数据库对象开发路径
     * <see cref="string"/>
     */
    private static String DBORootRelativePath;

    public static String getDBORootRelativePath() {
        return DBORootRelativePath;
    }

    public static void setDBORootRelativePath(String value) {
        DBORootRelativePath = value;
    }

    /**
     * 默认存放文件夹
     */
    public static String getDefaultFolder() {

        return "dbo";
    }
}
