package io.iec.edp.caf.databaseobject.api.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 数据库对象信息实体
 *
 * @author liu_wei
 */
@Data
@Entity
@Table(name = "GSPDatabaseObjectMapping")
public class GspDatabaseObjectMapping {

    /**
     * 唯一标识
     */
    @Id
    @Column(length = 36)
    private String id;


    /**
     * dboId
     */
    private String dboId;


    /**
     * 映射dboId
     */
    private String dboIdMapping;

}
