/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.context;


import io.iec.edp.caf.databaseobject.api.configuration.FileUtils;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * 数据库保留字
 *
 * @author liu_wei
 */
public class DatabaseReservedWords {
    private static ArrayList<String> reservedWords;

    /**
     * 数据库保留字列表
     */
    public static ArrayList<String> getReservedWords() {
        if (reservedWords == null || reservedWords.size() <= 0) {
            reservedWords = getReservedWordsFromConfig();
        }
        return reservedWords;
    }

    /**
     * 判断字符串是否是数据库保留字
     *
     * @param word
     * @return 如果是数据库保留字，返回true，否则，返回false
     * <see cref="bool"/>
     */
    public static boolean isReservedWord(String word) {
        if(!FileUtils.getReservedWordOpen())
        {
            return false;
        }
        return getReservedWords().contains(word.toUpperCase());
    }

    private static ArrayList<String> getReservedWordsFromConfig() {
        ArrayList<String> reservedWords = new ArrayList<String>(Arrays.asList(new String[]{"ADD", "ALL", "ALTER", "AND", "ANY", "AS", "ASC", "AUTHORIZATION", "AVG", "BACKUP", "BEGIN", "BETWEEN", "BREAK", "BROWSE", "BULK", "BY", "CASCADE", "CASE", "CHECK", "CHECKPOINT", "CLOSE", "CLUSTERED", "COALESCE", "COLLATE", "COLUMN", "COMMIT", "COMPUTE", "CONSTRAINT", "CONTAINS", "CONTAINSTABLE", "CONTINUE", "CONVERT", "COUNT", "CREATE", "CROSS", "CURRENT", "CURRENT_DATE", "CURRENT_TIME", "CURRENT_TIMESTAMP", "CURRENT_USER", "CURSOR", "DATABASE", "DATABASEPASSWORD", "DATEADD", "DATEDIFF", "DATENAME", "DATEPART", "DBCC", "DEALLOCATE", "DECLARE", "DEFAULT", "DELETE", "DENY", "DESC", "DISK", "DISTINCT", "DISTRIBUTED", "DOUBLE", "DROP", "DUMP", "ELSE", "ENCRYPTION", "END", "ERRLVL", "ESCAPE", "EXCEPT", "EXEC", "EXECUTE", "EXISTS", "EXIT", "EXPRESSION", "FETCH", "FILE", "FILLFACTOR", "FOR", "FOREIGN", "FREETEXT", "FREETEXTTABLE", "FROM", "FULL", "FUNCTION", "GOTO", "GRANT", "GROUP", "HAVING", "HOLDLOCK", "IDENTITY", "IDENTITY_INSERT", "IDENTITYCOL", "IF", "IN", "INDEX", "INNER", "INSERT", "INTERSECT", "INTO", "IS", "JOIN", "KEY", "KILL", "LEFT", "LIKE", "LINENO", "LOAD", "MAX", "MIN", "NATIONAL", "NOCHECK", "NONCLUSTERED", "NOT", "NULL", "NULLIF", "OF", "OFF", "OFFSETS", "ON", "ONLY", "OPEN", "OPENDATASOURCE", "OPENQUERY", "OPENROWSET", "OPENXML", "OPTION", "OR", "ORDER", "OUTER", "OVER", "PERCENT", "PLAN", "PRECISION", "PRIMARY", "PRINT", "PROC", "PROCEDURE", "PUBLIC", "RAISERROR", "READ", "READTEXT", "RECONFIGURE", "REFERENCES", "REPLICATION", "RESTORE", "RESTRICT", "RETURN", "REVOKE", "RIGHT", "ROLLBACK", "ROWCOUNT", "ROWGUIDCOL", "RULE", "SAVE", "SCHEMA", "SELECT", "SESSION_USER", "SET", "SETUSER", "SHUTDOWN", "SOME", "STATISTICS", "SUM", "SYSTEM_USER", "TABLE", "TEXTSIZE", "THEN", "TO", "TOP", "TRAN", "TRANSACTION", "TRIGGER", "TRUNCATE", "TSEQUAL", "UNION", "UNIQUE", "UPDATE", "UPDATETEXT", "USE", "USER", "VALUES", "VARYING", "VIEW", "WAITFOR", "WHEN", "WHERE", "WHILE", "WITH", "WRITETEXT", "ANALYSE", "ANALYZE", "ARRAY", "ASYMMETRIC", "BINARY", "BOTH", "CAST", "COLLATION", "CONCURRENTLY", "CURRENT_CATALOG", "CURRENT_ROLE", "CURRENT_SCHEMA", "DEFERRABLE", "DO", "FALSE", "FREEZE", "ILIKE", "INITIALLY", "ISNULL", "LATERAL", "LEADING", "LIMIT", "LOCALTIME", "LOCALTIMESTAMP", "NATURAL", "NOTNULL", "OFFSET", "ORDER", "OUTER", "OVERLAPS", "PLACING", "RETURNING", "SIMILAR", "SYMMETRIC", "TABLESAMPLE", "TRAILING", "TRUE", "USING", "VARIADIC", "VERBOSE", "WINDOW", "ACCESS", "AUDIT", "CHAR", "CLUSTER", "COLUMN_VALUE", "COMPRESS", "CONNECT", "DATE", "DECIMAL", "EXCLUSIVE", "FLOAT", "IDENTIFIED", "IMMEDIATE", "INCREMENT", "INITIAL", "INTEGER", "LEVEL", "LOCK", "LONG", "MAXEXTENTS", "MINUS", "MLSLABEL", "MODE", "MODIFY", "NESTED_TABLE_ID", "NOAUDIT", "NOCOMPRESS", "NOWAIT", "OFFLINE", "ONLINE", "PCTFREE", "PRIOR", "PRIVILEGES", "RAW", "RENAME", "RESOURCE", "ROW", "ROWID", "ROWNUM", "ROWS", "SESSION", "SHARE", "SIZE", "SMALLINT", "START", "SUCCESSFUL", "SYNONYM", "SYSDATE", "UID", "VALIDATE", "VARCHAR", "VARCHAR2", "WHENEVER", "ADMIN", "BIGDATEDIFF", "BYTE", "CALL", "CLUSTERBTR", "CONNECT_BY_ROOT", "CRYPTO", "CUBE", "DISKSPACE", "DOMAIN", "DROP", "EQU", "EXCEPTION", "EXCHANGE", "EXTRACT", "FIRST", "FULLY", "FUNCTION", "GROUP", "GROUPING", "INLINE", "INTERVAL", "KEEP", "LAST", "LINK", "LOGIN", "LIST", "LNNVL", "NEW", "NEXT", "NOCYCLE", "NVL", "OBJECT", "OUT", "OVERLAY", "PACKAGE", "PARTITION", "PENDANT", "REF", "REFERENCE", "REFERENCING", "REPEAT", "REPLICATE", "REVERSE", "ROLLUP", "SAVEPOINT", "SECTION", "SETS", "SIZEOF", "STATIC", "TIMESTAMPADD", "TIMESTAMPDIFF", "TRIM", "TYPEOF", "VERIFY", "VIRTUAL", "WITHIN", "INT", "TRXID", "VERSIONS_STARTTIME", "VERSIONS_ENDTIME", "VERSIONS_STARTTRXID", "VERSIONS_ENDTRXID", "VERSIONS_OPERATION"}));
        return reservedWords;
    }
}
