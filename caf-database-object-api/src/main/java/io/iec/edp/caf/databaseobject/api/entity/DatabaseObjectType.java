/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.entity;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

/**
 * 数据库对象类型
 *
 * @author liu_wei
 */
public enum DatabaseObjectType {
    /**
     * 表
     */
    Table(0),

    /**
     * 视图
     */
    View(1),

    /**
     * 函数
     */
    Function(2),

    /**
     * 字段
     */
    Column(3),

    /**
     * 索引
     */
    Index(4),

    /**
     * 存储过程
     */
    StoreProcedure(5),

    /**
     * 临时表
     */
    TempTable(6);

    public static final int SIZE = Integer.SIZE;

    private int intValue;
    private static Map<Integer, DatabaseObjectType> mappings;

    private static Map<Integer, DatabaseObjectType> getMappings() {
        if (mappings == null) {
            synchronized (DatabaseObjectType.class) {
                if (mappings == null) {
                    mappings = new HashMap<Integer, DatabaseObjectType>();
                }
            }
        }
        return mappings;
    }

    private DatabaseObjectType(int value) {
        intValue = value;
        getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static DatabaseObjectType forValue(int value) {
        return getMappings().get(value);
    }
}
