/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 数据库对象视图函数存储过程定义实体
 *
 * @author liu_wei
 */
@Data
public class DatabaseObjectViewDefinition implements Serializable {
    /**
     * 数据库类型
     */
    public String dbType;

    /**
     * 定义
     */
    public String definition;
}
