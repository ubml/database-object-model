/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.helper;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import io.iec.edp.caf.databaseobject.api.configuration.DatabaseObjectConfiguration;
import io.iec.edp.caf.databaseobject.api.configuration.FileUtils;
import io.iec.edp.caf.databaseobject.api.configuration.ObjectNameMaxLengthConfiguration;
import io.iec.edp.caf.databaseobject.api.configuration.TableNameConfiguration;
import io.iec.edp.caf.databaseobject.api.configuration.datatype.DatabaseObjectDataTypeConfiguration;
import io.iec.edp.caf.databaseobject.api.configuration.deplpyconfig.DboDeployConfiguration;
import io.iec.edp.caf.databaseobject.api.configuration.funcmapping.DatabaseFuncConfiguration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 配置帮助类
 *
 * @author liu_wei
 */
public class DboConfigurationHelper {
    ObjectMapper objectMapper = new ObjectMapper();

    /**
     * 构造函数
     */
    public DboConfigurationHelper() {
        objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.UPPER_CAMEL_CASE);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    /**
     * 获取表名配置
     *
     * @param filePath    路径
     * @param sectionName 节点
     * @return 表名配置列表
     * @throws IOException 异常
     */
    public List<TableNameConfiguration> getTableNameConfigurations(String filePath, String sectionName) throws IOException {
        String fileContent = FileUtils.fileRead(filePath);
        JsonNode objectNode = objectMapper.readTree(fileContent);
        String configuration = objectNode.findValue(sectionName).toString();
        return objectMapper.readValue(configuration, new TypeReference<ArrayList<TableNameConfiguration>>() {
        });
    }

    /**
     * 获取名称最大长度配置
     *
     * @param filePath    路径
     * @param sectionName 节点
     * @return 表名配置列表
     * @throws IOException 异常
     */
    public ObjectNameMaxLengthConfiguration getObjectNameMaxLengthConfiguration(String filePath, String sectionName) throws IOException {
        String fileContent = FileUtils.fileRead(filePath);
        JsonNode objectNode = objectMapper.readTree(fileContent);
        String configuration = objectNode.findValue(sectionName).toString();
        return objectMapper.readValue(configuration, ObjectNameMaxLengthConfiguration.class);
    }

    /**
     * 获取数据库类型配置
     *
     * @param filePath    路径
     * @param sectionName 节点
     * @return 表名配置列表
     * @throws IOException 异常
     */
    public ArrayList<DatabaseObjectDataTypeConfiguration> getDataTypeConfiguration(String filePath, String sectionName) throws IOException {
        String fileContent = FileUtils.fileRead(filePath);
        JsonNode objectNode = objectMapper.readTree(fileContent);
        String configuration = objectNode.findValue(sectionName).toString();
        return objectMapper.readValue(configuration, new TypeReference<ArrayList<DatabaseObjectDataTypeConfiguration>>() {
        });
    }

    /**
     * 获取功能配置
     *
     * @param filePath    路径
     * @param sectionName 节点
     * @return 表名配置列表
     * @throws IOException 异常
     */
    public ArrayList<DatabaseFuncConfiguration> getFuncConfiguration(String filePath, String sectionName) throws IOException {
        String fileContent = FileUtils.fileRead(filePath);
        JsonNode objectNode = objectMapper.readTree(fileContent);
        String configuration = objectNode.findValue(sectionName).toString();
        return objectMapper.readValue(configuration, new TypeReference<ArrayList<DatabaseFuncConfiguration>>() {
        });
    }

    /**
     * 获取SQL反射配置
     *
     * @param filePath    路径
     * @param sectionName 节点
     * @return 表名配置列表
     * @throws IOException 异常
     */
    public ArrayList<DatabaseObjectConfiguration> getSqlReflectConfiguration(String filePath, String sectionName) throws IOException {
        String fileContent = FileUtils.fileRead(filePath);
        JsonNode objectNode = objectMapper.readTree(fileContent);
        String configuration = objectNode.findValue(sectionName).toString();
        return objectMapper.readValue(configuration, new TypeReference<ArrayList<DatabaseObjectConfiguration>>() {
        });
    }

    /**
     * 获取DBO部署配置
     *
     * @param filePath    路径
     * @param sectionName 节点
     * @return 表名配置列表
     * @throws IOException 异常
     */
    public DboDeployConfiguration getDboDeployConfiguration(String filePath, String sectionName) throws IOException {
        String fileContent = FileUtils.fileRead(filePath);
        JsonNode objectNode = objectMapper.readTree(fileContent);
        String configuration = objectNode.findValue(sectionName).toString();
        return objectMapper.readValue(configuration, DboDeployConfiguration.class);
    }
}
