/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.service;

import io.iec.edp.caf.databaseobject.api.entity.AbstractDatabaseObject;


/**
 * @author liu_wei
 */
public interface IDatabaseObjectDtService {

    /**
     * 获取设计时DBO
     *
     * @param dboId id
     * @return DBO
     */
    AbstractDatabaseObject getDtDatabaseObject(String dboId);

    /**
     * 获取设计时DBO
     *
     * @param dboId id
     * @param su  服务单元
     * @return DBO
     */
    AbstractDatabaseObject getDtDatabaseObject(String dboId, String su);

    /**
     * 获取设计时DBO
     *
     * @param dboCode code
     * @return DBO
     */
    AbstractDatabaseObject getDtDatabaseObjectByCode(String dboCode);

    /**
     * 获取设计时DBO
     *
     * @param dboCode code
     * @param su  服务单元
     * @return DBO
     */
    AbstractDatabaseObject getDtDatabaseObjectByCode(String dboCode, String su);

    /**
     * 保存
     *
     * @param databaseObject DBO
     */
    void saveDtDatabaseObject(AbstractDatabaseObject databaseObject);

    /**
     * 保存
     *
     * @param databaseObject DBO
     * @param su  服务单元
     */
    void saveDtDatabaseObject(AbstractDatabaseObject databaseObject, String su);

    /**
     * 删除
     *
     * @param dboId id
     */
    void deleteDtDatabaseObject(String dboId);

    /**
     * 删除
     *
     * @param dboId id
     * @param su  服务单元
     */
    void deleteDtDatabaseObject(String dboId, String su);

    /**
     * 根据Id判断设计时DBO是否存在
     *
     * @param dboId id
     * @return 结果
     */
    boolean isExistDatabaseObjectDt(String dboId);

    /**
     * 根据Id判断设计时DBO是否存在
     *
     * @param dboId id
     * @param su  服务单元
     * @return 结果
     */
    boolean isExistDatabaseObjectDt(String dboId, String su);
}
