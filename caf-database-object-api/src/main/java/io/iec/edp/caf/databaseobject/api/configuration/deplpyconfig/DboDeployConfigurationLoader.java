/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.configuration.deplpyconfig;


import io.iec.edp.caf.databaseobject.api.configuration.FileUtils;
import io.iec.edp.caf.databaseobject.api.configuration.TableNameConfiguration;
import io.iec.edp.caf.databaseobject.api.helper.DboConfigurationHelper;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * 配置文件加载
 *
 * @author liu_wei
 */
public class DboDeployConfigurationLoader implements Serializable {
    private static String fileName = FileUtils.getSqlConfigPath();
    private static String sectionName = "DboDeployConfiguration";

    private static DboDeployConfiguration DatabaseObjectConfiguration;

    protected static DboDeployConfiguration getDatabaseObjectConfiguration() {
        return DatabaseObjectConfiguration;
    }

    private static void setDatabaseObjectConfiguration(DboDeployConfiguration value) {
        DatabaseObjectConfiguration = value;
    }

    /**
     * 构造函数
     */
    public DboDeployConfigurationLoader() {
        GetDatabaseObjectConfigurationData();
    }


    public static DboDeployConfiguration GetDatabaseObjectConfigurationData() {
        if (getDatabaseObjectConfiguration() == null) {
            DboConfigurationHelper helper = new DboConfigurationHelper();
            DboDeployConfiguration conf = null;

            try {
                conf = helper.getDboDeployConfiguration(fileName, sectionName);
            } catch (IOException e) {
                throw new RuntimeException("获取数据库对象部署配置报错", e);
            }
            setDatabaseObjectConfiguration(conf);
            return getDatabaseObjectConfiguration();
        } else {
            return getDatabaseObjectConfiguration();
        }
    }
}
