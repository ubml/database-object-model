/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.entity;

/**
 * DBO文件信息
 *
 * @author zhaoleitr
 */
public class DatabaseObjectInfo extends AbstractDatabaseObject {
    /**
     * 数据库对象全路径
     */
    private String filePath;

    public final String getFilePath() {
        return filePath;
    }

    public final void setFilePath(String value) {
        filePath = value;
    }
}
