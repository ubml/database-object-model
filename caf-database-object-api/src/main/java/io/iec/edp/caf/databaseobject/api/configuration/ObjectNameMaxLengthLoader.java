/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.configuration;

import io.iec.edp.caf.databaseobject.api.helper.DboConfigurationHelper;

import java.io.IOException;

/**
 * 对象名称最大长度加载器
 *
 * @author liu_wei
 */
public class ObjectNameMaxLengthLoader {
    private static String fileName = FileUtils.getSqlConfigPath();
    private static String sectionName = "ObjectNameMaxLength";

    private static ObjectNameMaxLengthConfiguration databaseObjectConfiguration;

    public static ObjectNameMaxLengthConfiguration getDatabaseObjectConfiguration() {
        return databaseObjectConfiguration;
    }

    public static void setDatabaseObjectConfiguration(ObjectNameMaxLengthConfiguration value) {
        databaseObjectConfiguration = value;
    }


    /**
     * 构造函数
     *
     * @throws IOException 异常
     */
    public ObjectNameMaxLengthLoader() throws IOException {
        GetDatabaseObjectConfiguration();
    }


    /**
     * 获取配置文件数据
     *
     * @return 最大名称长度
     * @throws IOException 异常
     */
    public static ObjectNameMaxLengthConfiguration GetDatabaseObjectConfiguration() throws IOException {
        if (getDatabaseObjectConfiguration() == null) {
            DboConfigurationHelper helper = new DboConfigurationHelper();
            ObjectNameMaxLengthConfiguration conf = null;

            conf = helper.getObjectNameMaxLengthConfiguration(fileName, sectionName);
            setDatabaseObjectConfiguration(conf);
            return getDatabaseObjectConfiguration();
        } else {
            return getDatabaseObjectConfiguration();
        }
    }
}
