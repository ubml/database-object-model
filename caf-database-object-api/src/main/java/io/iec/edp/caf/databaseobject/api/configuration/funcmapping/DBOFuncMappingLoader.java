/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.configuration.funcmapping;

import io.iec.edp.caf.databaseobject.api.configuration.FileUtils;
import io.iec.edp.caf.databaseobject.api.configuration.TableNameConfiguration;
import io.iec.edp.caf.databaseobject.api.helper.DboConfigurationHelper;

import java.io.IOException;
import java.util.*;

/**
 * 函数映射配置加载
 *
 * @author liu_wei
 */
public class DBOFuncMappingLoader {
    private static String fileName = FileUtils.getSqlConfigPath();
    private static String sectionName = "SysFuncConvert";


    private static List<DatabaseFuncConfiguration> databaseObjectConfigurations;

    private static List<DatabaseFuncConfiguration> getDatabaseObjectConfigurations() {
        return databaseObjectConfigurations;
    }

    private static void setDatabaseObjectConfigurations(List<DatabaseFuncConfiguration> value) {
        databaseObjectConfigurations = value;
    }

    /**
     * 构造函数
     */
    public DBOFuncMappingLoader() throws IOException {
        GetDatabaseObjectConfigurations();
    }


    /**
     * 获取配置文件数据
     *
     * @return 配置数据列表
     * @throws IOException 异常
     */
    public static List<DatabaseFuncConfiguration> GetDatabaseObjectConfigurations() throws IOException {
        if (getDatabaseObjectConfigurations() == null) {
            DboConfigurationHelper helper = new DboConfigurationHelper();
            ArrayList<DatabaseFuncConfiguration> confs = null;

            confs = helper.getFuncConfiguration(fileName, sectionName);

            setDatabaseObjectConfigurations(confs);
            return getDatabaseObjectConfigurations();
        } else {
            return getDatabaseObjectConfigurations();
        }
    }

    /**
     * 根据功能名称获取配置
     *
     * @param funcName 功能名称
     * @return 配置数据
     * @throws IOException 异常
     */
    public static DatabaseFuncConfiguration GetDatabaseObjectConfiguration(String funcName) throws IOException {
        GetDatabaseObjectConfigurations();
        if (getDatabaseObjectConfigurations() != null && !getDatabaseObjectConfigurations().isEmpty()) {
            for (DatabaseFuncConfiguration data : getDatabaseObjectConfigurations()) {
                if (data.getFuncName().equals(funcName)) {
                    return data;
                }
            }
            return null;
        } else {
            return null;
        }
    }
}
