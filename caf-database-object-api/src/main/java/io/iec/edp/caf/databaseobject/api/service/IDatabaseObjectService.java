/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.service;

import io.iec.edp.caf.databaseobject.api.entity.AbstractDatabaseObject;
import io.iec.edp.caf.databaseobject.api.entity.DBInfo;
import io.iec.edp.caf.databaseobject.api.entity.DatabaseObjectInfo;
import io.iec.edp.caf.databaseobject.api.entity.DatabaseObjectTable;

import java.util.List;

/**
 * BDO服务接口
 *
 * @author zhaoleitr
 */
public interface IDatabaseObjectService {
    /**
     * 根据全路径获取数据库对象详细信息
     *
     * @param fullPath 全路径
     * @return 数据库对象详细信息
     */
    AbstractDatabaseObject getDatabaseObject(String fullPath);

    /**
     * 根据全路径获取数据库对象详细信息列表
     *
     * @param fullPath 全路径
     * @return 数据库对象详细信息列表
     */
    List<AbstractDatabaseObject> getDatabaseObjectListRecursivly(String fullPath);


    /**
     * 根据文件夹路径获取所有dbo
     *
     * @param fullPath 文件夹路径
     * @return dbo列表
     */
    List<DatabaseObjectInfo> getDatabaseObjectList(String fullPath);

    /**
     * 保存DBO
     *
     * @param fullPath       完整路径
     * @param databaseObject dbo实体
     */
    void saveDatabaseObjectTableWithoutFileName(String fullPath, DatabaseObjectTable databaseObject);

    /**
     * 判断dbo是否存在
     *
     * @param fullPath 完整路径
     * @param code     code
     * @param dboId    id
     * @return 是否
     */
    boolean isExistDatabaseObject(String fullPath, String code, String dboId);

    /**
     * 获取DBO文件相关信息，不存在返回null
     *
     * @param fullPath 完整路径
     * @param code     code
     * @param dboId    id
     * @return DBO文件存储信息
     */
    DatabaseObjectInfo getDatabaseObjectInfo(String fullPath, String code, String dboId);

    /**
     * 根据路径和id获取dbo实体
     *
     * @param fullPath         完整路径
     * @param databaseObjectId id
     * @return dbo实体
     */
    AbstractDatabaseObject getDatabaseObjectById(String fullPath, String databaseObjectId);

    /**
     * 获取当前环境数据库信息
     *
     * @return 当前环境的数据库信息
     */
    DBInfo getDbInfo();

}
