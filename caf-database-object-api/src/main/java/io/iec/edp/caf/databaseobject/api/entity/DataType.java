/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.entity;

/**
 * 数据类型
 *
 * @author liu_wei
 */
public enum DataType {
    /**
     * 字符型
     */
    Char,

    /**
     * 可变长字符型
     */
    Varchar,

    /**
     * 长文本
     */
    Clob,

    /**
     * 整型
     */
    Int,

    /**
     * 十进制
     */
    Decimal,

    /**
     * 日期型
     */
    DateTime,

    /**
     * 日期时间型
     */
    TimeStamp,

    /**
     * 大二进制
     */
    Blob,

    /**
     * Unicode字符数据类型
     */
    NChar,

    /**
     * Unicode字符串数据类型
     */
    NVarchar,

    /**
     * Unicode长文本
     */
    NClob,

    /**
     * Bool类型
     */
    Boolean,

    /**
     * 数字
     */
    SmallInt,

    /**
     * 长整型数据
     */
    LongInt,

    /**
     * 浮点型
     */
    Float,

    /**
     * 暂未支持类型
     */
    NotSupported,

    /**
     * 产品需求，兼容pg的Jsonb类型，仅作为标识
     */
    Jsonb;


    public static final int SIZE = Integer.SIZE;

    public int getValue() {
        return this.ordinal();
    }

    public static DataType forValue(int value) {
        return values()[value];
    }
}
