/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.databaseobject.api.entity;

/**
 * 数据库连接信息
 *
 * @author liu_wei
 */
public class DBInfo {

    /**
     * 数据库类型
     */
    private DbType dbType;

    public DbType getDbType() {
        return dbType;
    }

    public void setDbType(DbType dbType) {
        this.dbType = dbType;
    }

    /**
     * 数据库地址
     */
    private String server;

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    /**
     * 数据库名称
     */
    private String dbName;

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    /**
     * 数据库账号
     */
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 数据库密码
     */
    private String passWord;

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    /**
     * 端口
     */
    private String port;

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    /**
     * 数据库连接串
     */
    private String url;

    public String getUrl() {
        if (url == null || url.length() <= 0) {
            switch (dbType) {
                case DM:
                    url = "jdbc:dm://" + server + ":" + port + "/DMSERVER?schema=" + dbName;
                    break;
                case PgSQL:
                    url = "jdbc:postgresql://" + server + ":" + port + "/" + dbName;
                    break;
                case Oracle:
                    url = "jdbc:oracle:thin:@//" + server + ":" + port + "/" + dbName;
                    break;
                case SQLServer:
                    url = "jdbc:sqlserver://" + server + "\\" + dbName + ":" + port + ";database=" + dbName;
                    break;
                case HighGo:
                    url = "jdbc:highgo://" + server + ":" + port + "/" + dbName;
                    break;
                case MySQL:
                    url = "jdbc:mysql://" + server + ":" + port + "/" + dbName + "?characterEncoding=utf8&serverTimezone=UTC&useSSL=false";
                    break;
                case Oscar:
                    url = "jdbc:oscar://" + server + ":" + port + "/" + dbName;
                    break;
                case Kingbase:
                    url = "jdbc:kingbase8://" + server + ":" + port + "/" + dbName;
                    break;
                case DB2:
                    url = "jdbc:db2://" + server + ":" + port + "/" + dbName;
                    break;
                case OpenGauss:
                    url = "jdbc:opengauss://" + server + ":" + port + "/" + dbName+"?batchMode=off&loggerLevel=OFF";
                    break;
                case GBase8s:
                    url = "jdbc:gbase8s://" + server + ":" + port + "/" + dbName+"?batchMode=off&loggerLevel=OFF";
                    break;
                case GBase8c:
                    url = "jdbc:gbase8c://" + server + ":" + port + "/" + dbName+"?batchMode=off&loggerLevel=OFF";
                    break;
                default:
                    throw new RuntimeException("数据库类型不正确");
            }
        }
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 模式
     */
    private String schema;

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }
}
