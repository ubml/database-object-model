/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.api.configuration.deplpyconfig;

import io.iec.edp.caf.commons.runtime.CafEnvironment;
import lombok.Data;

import java.nio.file.Paths;

/**
 * DBO部署配置
 *
 * @author liu_wei
 */
@Data
public class DboDeployConfiguration {
    private String classPath;

    public String getClassPath() {
        return Paths.get(CafEnvironment.getServerRTPath(), classPath).toString();
    }

    private String javaPath;

    public String getJavaPath() {
        return Paths.get(CafEnvironment.getServerRTPath(), javaPath).toString();
    }

    private String jarPath;

    public String getJarPath() {
        return Paths.get(CafEnvironment.getServerRTPath(), jarPath).toString();
    }
}
