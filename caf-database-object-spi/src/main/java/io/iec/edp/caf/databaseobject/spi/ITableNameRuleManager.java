/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.spi;

import io.iec.edp.caf.databaseobject.api.entity.DBOTableNameRule;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author liu_wei
 */
public interface ITableNameRuleManager {

    String getTableName(String tableName, DBOTableNameRule rule, Map<String, String> dimensionInfo);


    List<String> getTableNames(String tableName, DBOTableNameRule rule, Map<String, List<String>> dimensioinInfo);
}
